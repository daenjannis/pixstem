.. _api:

=================
API documentation
=================

Classes
-------

PixelatedSTEM
=============
.. autoclass:: pixstem.pixelated_stem_class.PixelatedSTEM
    :members:
    :undoc-members:

DPCBaseSignal
=============
.. autoclass:: pixstem.pixelated_stem_class.DPCBaseSignal
    :members:
    :undoc-members:

DPCSignal1D
===========
.. autoclass:: pixstem.pixelated_stem_class.DPCSignal1D
    :members:
    :undoc-members:

DPCSignal2D
===========
.. autoclass:: pixstem.pixelated_stem_class.DPCSignal2D
    :members:
    :undoc-members:

Modules
-------

IO tools
========
.. automodule:: pixstem.io_tools
    :members:
    :undoc-members:

Radial
======
.. automodule:: pixstem.radial
    :members:
    :undoc-members:


Fluctuation electron microscopy analysis
========================================
.. automodule:: pixstem.fem_tools
    :members:
    :undoc-members:

Marker tools
============
.. automodule:: pixstem.marker_tools
    :members:
    :undoc-members:


Dummy data
==========
.. automodule:: pixstem.dummy_data
    :members:
    :undoc-members:

Make diffraction test data
==========================
.. automodule:: pixstem.make_diffraction_test_data
    :members:
    :undoc-members:

